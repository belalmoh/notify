import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import NotifyApp from './src/App'

ReactDOM.render(
    <BrowserRouter>
        <NotifyApp />
    </BrowserRouter>,
    document.getElementById('app')
);