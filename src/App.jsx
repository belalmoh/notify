import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';
import RouterNavigation from './navigation/router';

import Store from './config/store'

export default class App extends React.Component {
    render(){
        return (
            <Provider store={Store}>
                <BrowserRouter>
                    <RouterNavigation />
                </BrowserRouter>
            </Provider>
        )
    }
}