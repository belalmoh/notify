import React from 'react'
import {Link, Route, Switch} from 'react-router-dom'

import LandingPage from 'Components/landing'
import SignUp from 'Containers/signup'

export default class App extends React.Component {
    render() {
        const {match} = this.props;
        return (
            <div>
            <LandingPage 
                title={"NotifyApp"} 
                match={match}
                buttons={
                    [
                        {name: 'Sign up', color:'inherit', to: `${match.url}/signup`}
                    ]
                }/>
                <Route path={`${match.path}/signup`} component={SignUp} />
            </div>
        )
    }
}