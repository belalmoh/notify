import signupActions, {
  reducer as signupReducer,
  signupTypes
} from './state';

const signup = {
  reducer: signupReducer,
  types: signupTypes,
  actions: signupActions,
  // sagas
  // selectors
}

export default signup;