import { createReducer, createActions } from 'reduxsauce'

export const INITIAL_STATE = { };


// TYPES
// -------

const {Types, Creators} = createActions({
    saveForm: ['userName', 'password']
});

export const signupTypes = Types;
export default Creators;


const saveForm = (state, action) => {
    return state
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SAVE_FORM]: saveForm,
});