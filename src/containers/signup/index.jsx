import React, {Component} from 'react'
import * as Yup from 'yup'

import Dialog from 'Components/dialog'
import Form from 'Components/form'

let Objectify = array => {
    let result = {}
    for(var i = 0; i < array.length; i++) {
        result[Object.keys(array[i])] = Object.values(array[i])[0]
    }
    return result
}

const METADATA = {
    fields: {
        email: {label: "Email", name: "email", type: "text"},
        username: {label: "User name", name: "username", type: "text"},
        password: {label: "Password", name: "password", type: "password"},
        passwordConfirm: {label: "Confirm Password", name: "passwordConfirm", type: "password"}
    },
    validationSchema: {
        email: Yup.string().email("Invalid Email Address").required(),
        username: Yup.string().min(3).required(),
        password: Yup.string().matches(/^[(A-Za-z)|(a-zA-Z)\d\D]{10,}$/, "Password should be more than 10 (low/up case and numbers should be included)").required(),
        passwordConfirm: Yup.string().oneOf([Yup.ref('password'), null], "Confirmation Password should match Password's value").required()
    }
}

let generateFormFields = fieldsParam => {
    // will improve it later.
    return {
        fields: Objectify(fieldsParam.map(({name}) => {return {[name]: ""}})),
        fieldsMetaData: Objectify(fieldsParam.map(({name, type}) => {return {[name]: METADATA.fields[type]}})),
        validationSchema: Yup.object().shape(Objectify(fieldsParam.map(({name, type}) => {return {[name]: METADATA.validationSchema[type]}})))
    }
}

export default class SignUp extends Component {
    PAGE_INFO = {
        title: "Signup Page"
    }

    toggleVisibility = () => {
        const {history: {goBack}} = this.props
        goBack();
    }

    render(){

        const {fields, fieldsMetaData, validationSchema} = generateFormFields([{name: "email", type: "email"}, {name: "username", type: "username"}, {name: "password", type: "password"}, {name: "passwordConfirm", type: "passwordConfirm"}])
        return (
            <Dialog 
                visible={true}
                title={this.PAGE_INFO.title}
                onClose={this.toggleVisibility}
            >
            <Form 
            fields={fields}
            fieldsMetaData={fieldsMetaData}
            validationSchema={validationSchema}
            />
            </Dialog>
        )
    }
}