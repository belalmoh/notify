import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import SignupDucks from '../containers/signup/ducks'

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  signup: SignupDucks.reducer
});

export default createStore(reducers, composeWithDevTools(
  applyMiddleware(sagaMiddleware)
));