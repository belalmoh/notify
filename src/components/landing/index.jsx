import React, {Component} from 'react'
import PropTypes from 'prop-types'
import shortid from 'shortid'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import Image from '../image'
 
import LandingImage from 'Images/landing.jpg'
import Logo from 'Images/logo.png'

import LinkButton from 'Components/link-button'

import Style from './style.scss'

export default class Landing extends Component {
    
    static propTypes = {
        title: PropTypes.string.isRequired,
        buttons: PropTypes.arrayOf(PropTypes.object)
    }

    static defaultProps = {
        title: 'NotifyApp',
        buttons: []
    }
    
    render(){
        const {title, buttons} = this.props
        return (
            <div className="landing-container">
                <Image image={LandingImage} type={"landing"}>
                    <AppBar position="static" color="primary" className="appbar">
                        <Toolbar>
                        <Image image={Logo} type={"logo"} height={4} width={4} className="image"/>
                        <Typography variant="title" color="inherit" className="title-flex1">
                            {title}
                        </Typography>
                        {buttons.map((button) => {
                            return (
                                <LinkButton key={shortid.generate()} {...button}>{button.name}</LinkButton>
                            )
                        })}
                        </Toolbar>
                    </AppBar>
                </Image>
            </div>
        );
    }
}