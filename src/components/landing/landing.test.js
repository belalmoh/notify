import React from 'react'
import Landing from './index'
import renderer from 'react-test-renderer'

describe('Landing page test', () => {
    it('should render the page successfully', () => {
        const component = renderer.create(
            <Landing />
        )

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })
})