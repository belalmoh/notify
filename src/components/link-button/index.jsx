import React, {Component} from 'react'
import {withRouter} from 'react-router'
import Button from '@material-ui/core/Button';

class LinkButton extends Component {
    render(){
        const {
            history,
            location,
            match,
            staticContext,
            to,
            onClick,
            title,
            ...rest
        } = this.props
        return (
            <Button {...rest}
            onClick={(event) => {
              onClick && onClick(event)
              history.push(to)
            }}/>
        )
    }
}

export default withRouter(LinkButton)