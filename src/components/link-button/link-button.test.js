import React from 'react'
import LinkButton from './index'
import renderer from 'react-test-renderer'
import {MemoryRouter} from 'react-router-dom'

describe('Link Button Component test', () => {
    it('should render button successfully', () => {
        const component = renderer.create(
            <MemoryRouter>
                <LinkButton to={"anywhere/there"}>Test</LinkButton>
            </MemoryRouter>
        )

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })
})