import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import * as Yup from 'yup'
import { Formik } from 'formik';

export default class FormikForm extends Component {
    
    static propTypes = {
        fields: PropTypes.object.isRequired,
        fieldsMetaData: (props) => {
            if(JSON.stringify(Object.keys(props.fields)) !== JSON.stringify(Object.keys(props.fieldsMetaData)))
                return new Error("Meta data of fields is incorrect")
        },
        validationSchema: PropTypes.object
    }

    static defaultProps = {
        fields: {},
        validationSchema: {},
        validationSchema: {}
    }
    
    render() {
        const {fields, validationSchema, fieldsMetaData} = this.props
        return (
            <Formik
            initialValues={fields}
            validationSchema={validationSchema}
            onSubmit={(
                values,
                { setSubmitting, setErrors /* setValues and other goodies */ }
            ) => {
                console.log("HERE")
                setSubmitting(true)
            }}
            render={({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                setFieldValue,
                ...otherProps
            }) => {
                return (
                    <form onSubmit={handleSubmit}>
                    {Object.keys(values).map((obj, key) => {
                        return (
                            <div key={`${obj}${key}`}>
                                <TextField
                                type={fieldsMetaData[obj].type}
                                label={fieldsMetaData[obj].label}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values[obj]}
                                name={fieldsMetaData[obj].name}
                                />
                                {errors[obj] && touched[obj] &&<p>{errors[obj]}</p>}
                            </div>
                        )
                    })}
                    <button type="submit" disabled={isSubmitting}>
                        Submit
                    </button>
                    </form>
            )}}
            />
        )
    }
}