import React, {Component} from 'react'

import PropTypes from 'prop-types'

import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

export default class CustomDialog extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        visible: PropTypes.bool.isRequired,
        onClose: PropTypes.func.isRequired,
        children: PropTypes.element
    }

    static defaultProps = {
        title: "",
        visible: true,
        onClose: () => {},
        children: <div></div>
    }

    render(){
        const {title, visible, onClose, children} = this.props
        return (
            <div>
                <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={visible}>
                    <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
                    {children}
                </Dialog>
            </div>
        )
    }
}