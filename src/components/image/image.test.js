import React from 'react'
import Image from './index'
import renderer from 'react-test-renderer'

import LandingImage from '../../../assets/images/landing.jpg'
import Logo from '../../../assets/images/logo.png'

describe('Testing of image component', () => {
    it('handling the rendering of type => logo', () => {
        const component = renderer.create(
            <Image image={Logo} type={"logo"} height={4} width={4} className="image" />
        )

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('handling the rendering of type => landing', () => {
        const component = renderer.create(
            <Image image={LandingImage} type={"landing"}>
                <div></div>
            </Image>
        )

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('handling the rendering of type => default', () => {
        const component = renderer.create(
            <Image image={LandingImage}>
            </Image>
        )

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })
})