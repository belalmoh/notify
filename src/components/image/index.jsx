import React, {Component} from 'react'
import PropTypes from 'prop-types'
import requiredIf from 'react-required-if';

import './style.scss'

export default class Image extends Component {
    
    static propTypes = {
        image: PropTypes.string.isRequired,
        height: PropTypes.number,
        width: PropTypes.number,
        type: PropTypes.oneOf(Object.keys(Image.Types())).isRequired,
        isResponsive: PropTypes.bool,
        children: requiredIf(PropTypes.node, props => props.type.toLowerCase() === 'landing'),
        className: PropTypes.string
    }

    static defaultProps = {
        image: "",
        height: 100,
        width: 100,
        type: "default",
        isResponsive: false,
        className: ""
    }

    static Types(props=null) {
        return {
            landing: () => {
                return (
                    <div className="landing-container">
                        {props.children}
                        <img src={props.image} height={`${props.height}%`} width={`${props.width}%`} className="landing-image background-image" />
                    </div>
                )
            },
            logo: () => {
                return (
                    <img src={props.image} height={`${props.height}%`} width={`${props.width}%`} className={props.className} />
                )
            },
            image: {},
            icon: {},
            default: () => {
                return (
                    <img src={props.image} height={`${props.height}%`} width={`${props.width}%`} className={props.className} />
                )
            }
        }
    }

    render(){
        const { image, height, width, type, isResponsive, className, children} = this.props
        
        return (
            Image.Types({ image, height, width, type, isResponsive, className, children })[type.toLowerCase()]()
        )
    }
}