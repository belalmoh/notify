import React from 'react'
import { Route, Redirect, BrowserRouter as Router, Link, Switch } from 'react-router-dom'

import LandingPage from 'Containers/landing-page'
import SignUp from 'Containers/signup'

export default () => (
    <Router>
        <div>
            <Router path="/" component={() => <h1>HOME</h1>} />
            <Route path="/landing" component={LandingPage} />
        </div>
    </Router>
)