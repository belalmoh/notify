const path = require("path");
const webpack = require("webpack");
const webpack_rules = []

const babelReactRule = {
    test: /(\.js$)|(\.jsx$)/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: "babel-loader",
        options: {
            presets: ["@babel/preset-env", "@babel/preset-react"]
        }
    }
}

const sassRule = {
    test: /(\.scss$)|(\.sass$)|(.css$)/,
    use: [
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS
    ]
}

const imagesRule = {
    test: /\.(png|svg|jpg|gif)$/,
    use: ['file-loader']
}

const webpackOption = {
    entry: "./app.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Images: path.resolve(__dirname, "assets/images"),
            Containers: path.resolve(__dirname, "src/containers"),
            Components: path.resolve(__dirname, "src/components"),
        }
    },
    mode: 'development',
    module: {
        rules: webpack_rules
    },
    devtool: 'source-map',
    devServer: {
        contentBase: './dist',
        port: 9000,
        stats : "errors-only",
        historyApiFallback: true,
        open: true
    }
};

webpack_rules.push(babelReactRule, sassRule, imagesRule)
module.exports = webpackOption;